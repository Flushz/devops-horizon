FROM nguyenthai23/ruby-ubuntu:1.0
WORKDIR /app
COPY . .
RUN bundle install 
CMD [ "bundle", "exec", "jekyll", "server" ]